const staticCacheName = 'my-mzpbeling';

self.addEventListener("install", event => {
  console.log('Installing...');
  event.waitUntil(
    caches.open(staticCacheName).then(cache => {
      return cache
        .addAll([
          "/",
          "/static/manifest.json",
          "/static/css/profil.css",
          "/static/css/menu.css",
          "/static/css/body.css",
          "/static/css/topbar-menu.css",
          "/static/css/card.css",
          "/static/css/font-awesome.min.css",
          "/static/css/animated.css",
          "/static/css/input.css",
          "/static/css/table.css",
          "/static/css/tabs.css",
          "/static/css/404.css",
          "/static/js/menu.js",
          "/static/fonts/fontawesome-webfont.eot",
          "/media/img/circle-cropped.png"
        ])
        .catch(error => {
          console.log("Caches open failed: " + error);
        });
    })
  );
});

self.addEventListener('activate', function(event) {
    console.log('Activating...');

    // Update all caches
    event.waitUntil(
        caches.keys().then(function(cacheNames) {
            return Promise.all(
                cacheNames.filter(function(cacheName) {
                    return cacheName.startsWith('my-') && cacheName != staticCacheName;
                }).map(function(cacheName) {
                  return caches.delete(cacheName);
                })
            );
        })
    );
});

self.addEventListener('fetch', function(event) {
    console.log('Fetching...');

    event.respondWith(
        caches.match(event.request).then(function(response){
            //console.log(response);

            return response || fetch(event.request).then(function(response) {
                return caches.open(staticCacheName).then(function(cache) {
                    cache.put(event.request, response.clone());
                    return response;
                });
            })
        })
    );
});