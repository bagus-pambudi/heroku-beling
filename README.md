# Welcome to Tugas 2 Beling - by bastian

### setup your environment with python3.6
> virtualenv ../venv -p python3.6
> source ../venv/bin/activate

### install package 
> pip install -r requirements.txt

## to running application in your local
> python manage.py runserver

### migrate database
> python manage.py makemigrations

> python manage.py migrate

> python manage.py createsuperuser
