from django.shortcuts import render

# Create your views here.
def index(request):
    title = "Login"
    response = {'title': title }
    html = 'tutorial6/login.html'
    return render(request, html, response)
