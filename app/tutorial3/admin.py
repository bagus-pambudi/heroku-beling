from django.contrib import admin
from .models import MyExperience
from import_export.admin import ImportExportModelAdmin

# Register your models here.
class ExperienceAdmin(ImportExportModelAdmin):
    list_display = ('jabatan', 'tempat', 'wilayah','tanggal_mulai', 'tanggal_akhir', 'status_akhir')
    list_filter = ('status_akhir',)
    search_fields = ['jabatan', 'tempat', 'url_tempat', 'wilayah', 'status_akhir', 'keterangan', ]
    list_per_page = 5
admin.site.register(MyExperience, ExperienceAdmin)
