from django.db import models
from datetime import datetime

# Create your models here.
class MyExperience(models.Model):
    jabatan = models.CharField(max_length=100)
    tempat = models.CharField(max_length=100)
    url_tempat = models.CharField(max_length=100)
    wilayah = models.CharField(max_length=50)
    logo = models.FileField(upload_to='upload/my_experience/', blank=True, null=True)
    tanggal_mulai = models.DateField(help_text='yyyy-mm-dd')
    tanggal_akhir = models.DateField(blank=True, null=True, help_text='yyyy-mm-dd')
    status_akhir = models.BooleanField()
    keterangan = models.TextField()

    def get_start(self):
        return self.tanggal_mulai.year
        
    def get_end(self):
        return self.tanggal_akhir.year

    def __str__(self):
        return self.jabatan

    class Meta:
        verbose_name = 'Data Experience'
        ordering = ["tanggal_mulai"]
