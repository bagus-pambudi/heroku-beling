from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse
from .models import MyExperience

# Create your views here.
def my_experience(request):
    title = 'Experience'
    exp = MyExperience.objects.all()
    response = {'title' : title }
    return render(request, 'my_experience/my_experience.html', response)