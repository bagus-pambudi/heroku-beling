from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import Message_Forms
from .models import Message

# Create your views here.
def index(request):
    response = {'message_form': Message_Forms}
    html = 'tutorial4/tutorial4.html'
    return render(request, html, response)

def message_post(request):
    form = Message_Forms(request.POST or None)
    if(request.method ==  'POST' and form.is_valid()):
        # get value from post
        response = {}
        response['name'] =  request.POST['name'] if request.POST['name'] != "" else "Anonymus"
        response['email'] =  request.POST['email'] if request.POST['email'] != "" else "Anonymus"
        response['message'] =  request.POST['message']
        # insert to database
        message =  Message(name = response['name'], email= response['email'], message = response['message'])
        message.save()
        html = 'tutorial4/form_result.html'
        return render(request, html, response)
    else:
        return HttpResponseRedirect('/tutorial4/')

def message_table(request):
    message = Message.objects.all()
    response = { 'message' : message }
    html = 'tutorial4/tables.html'
    return render(request, html , response)