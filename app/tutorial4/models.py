from django.db import models
# from datetime import datetime
from django.utils import timezone

# Create your models here.

class AutoDateTimeField(models.DateTimeField):
    def pre_save(self, model_instance, add):
        return timezone.now()

class Message(models.Model):
    name = models.CharField(max_length=27)
    email = models.EmailField(max_length=254)
    message = models.TextField()
    created_date = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return self.message

    class Meta:
        verbose_name = 'Data Message'