from django.urls import path
from .views import index, message_post, message_table

urlpatterns = [
    path(r'', index, name='message_index'),
    path(r'message/post', message_post, name='message_post'),
    path(r'table', message_table, name='message_table'),
    
]


