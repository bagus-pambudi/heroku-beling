from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse
from datetime import date
from app.frontend.utils import utils

# Create your views here.
name = 'Bagus Pambudi - Bastian'
birthdatetxt = '28 Oktober 1993'
email = 'maspamzdev@gmail.com'
birthdate = date(1993, 10, 28)
place = 'Tangerang'
university = 'S1 - Teknik Informatika - STMIK Masa Depan'
job = 'IT Support'
hope = 'Ingin memperdalam framework Django, dan mempunyai komunitas \n serta teman yang suka dengan framework Django'
def tutorial2(request):
    title = 'Django - Template'
    response = {'title':title, 'name' : name, 'age': utils.calc_age(birthdate),
     'birthdate':birthdatetxt,  'place': place, 'university': university, 'job':job, 'hope': hope }
    return render(request, 'frontend/app/tutorial2.html', response)