from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse
from .models import Profil, ListTask
from datetime import date
from .utils import utils

# Create your views here.
name = 'Bagus Pambudi - Bastian'
birthdatetxt = '28 Oktober 1993'
email = 'maspamzdev@gmail.com'
birthdate = date(1993, 10, 28)
place = 'Tangerang'
university = 'S1 - Teknik Informatika - STMIK Masa Depan'
job = 'IT Support'
hope = 'Ingin memperdalam framework Django, dan mempunyai komunitas \n serta teman yang suka dengan framework Django'

def home(request):
    title = 'Home'
    pf = Profil.objects.all().order_by('id')
    response = {'name' : name, 'age': utils.calc_age(birthdate),
     'birthdate':birthdatetxt,  'place': place, 'university': university, 
     'job':job, 'hope': hope, 'pf' : pf, 'title' : title }
    return render(request, 'frontend/app/home.html', response)

def service_worker(request, js):
      template = get_template('service-worker.js')
      html = template.render()
      return HttpResponse(html, content_type="application/x-javascript")

# for csrf while error
def csrf_failure(request, reason=""):
    ctx = {'message': 'some custom messages'}
    return render_to_response(your_custom_template, ctx)