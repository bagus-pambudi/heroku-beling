from django.contrib import admin
from .models import Profil, ListTask
from import_export.admin import ImportExportModelAdmin

# Register your models here.
class ProfilAdmin(ImportExportModelAdmin):
    list_display = ('name', 'email', 'photo', 'description', )
    search_fields = ['name', 'email', 'photo', 'description', ]
admin.site.register(Profil, ProfilAdmin)

class ListTaskAdmin(ImportExportModelAdmin):
    list_display = ('title', 'url', 'description', )
    search_fields = ['title', 'url', 'description', ]
admin.site.register(ListTask, ListTaskAdmin)