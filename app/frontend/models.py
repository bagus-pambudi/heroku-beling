from django.db import models

# Create your models here.
class Profil(models.Model):
    name = models.CharField(max_length=50)
    email = models.CharField(max_length=50)
    photo = models.FileField(upload_to='img/', blank=True, null=True)
    description = models.TextField()

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Data Profil'

class ListTask(models.Model):
    title = models.CharField(max_length=50)
    url = models.CharField(max_length=50)
    description = models.TextField()

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Data Tugas'
