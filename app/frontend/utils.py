from datetime import datetime, date

class utils():
    def calc_age(age):
        today = date.today()
        my_age = today.year - age.year - ((today.month, today.day) < (age.month, age.day)) 
        return my_age