from django.urls import path
from .views import index, save

urlpatterns = [
    path(r'', index, name='chat_index'),
    path(r'save', save, name='chat_save'),
]


