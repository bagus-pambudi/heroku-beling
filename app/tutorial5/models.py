from django.db import models
from django.utils import timezone

# Create your models here.
class AutoDateTimeField(models.DateTimeField):
    def pre_save(self, model_instance, add):
        return timezone.now()

class Chat(models.Model):
    nama = models.CharField(max_length=50)
    pesan = models.TextField()
    created_date = models.DateTimeField(default=timezone.now)
    
    def __str__(self):
        return self.nama