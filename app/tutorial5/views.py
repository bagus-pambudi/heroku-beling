from django.shortcuts import render
from django.http import HttpResponseRedirect
from .models import Chat

# Create your views here.
def index(request):
    title = "Chat"
    chat = Chat.objects.all().order_by('-created_at')
    response = {'title': title, 'chat': chat}
    html = 'tutorial5/chat.html'
    return render(request, html, response)

def save(request):
    if(request.method ==  'POST'):
        # get value from post
        response = {}
        response['nama'] =  request.POST['nama'] if request.POST['nama'] != "" else "Anonymus"
        response['pesan'] =  request.POST['pesan'] if request.POST['pesan'] != "" else "Anonymus"
        # insert to database
        chat =  Chat(nama = response['nama'], pesan= response['pesan'])
        caht.save()
        html = 'tutorial5/chat.html'
        return render(request, html, response)
    else:
        return HttpResponseRedirect('/tutorial5/')
